//Inicializador del elemento Slider
$("#rangoPrecio").ionRangeSlider({
  type: "double",
  grid: false,
  min: 0,
  max: 100000,
  from: 1000,
  to: 20000,
  prefix: "$"
})
var filtro={
  todo:true,
  ciudad:"",
  tipo:"",
  precioMin:0,
  precioMax:100000
}
function setSearch() {
  let busqueda = $('#checkPersonalizada')
  busqueda.on('change', (e) => {
    if (this.customSearch == false) {
      this.customSearch = true
      filtro.todo=true
      filtro.ciudad=""
      filtro.tipo=""
      filtro.precioMin=0
      filtro.precioMax=100000
    } else {
      this.customSearch = false
      filtro.todo=false
      cargarMenus();
    }
    $('#personalizada').toggleClass('invisible')
  })
}

setSearch()



function cargarMenus(){
    $.ajax({
      type: "POST",
      url: "/Cargar_ciudad",
      success: function(data) {
        document.getElementById("ciudad").innerHTML=data
      }
    });
  $.ajax({
    type: "POST",
    url: "/Cargar_tipo",
    success: function(data) {
      document.getElementById("tipo").innerHTML=data
    }
    });
}
$('#buscar').click(()=>{
if(filtro.todo==false){
filtro.precioMin=document.querySelector(".irs-from").innerHTML.replace("$","").replace(" ","")
filtro.precioMax=document.querySelector(".irs-to").innerHTML.replace("$","").replace(" ","")
}
console.log(filtro);
      $.ajax({
        type: "POST",
        url: "/Mostar_todo",
        data: filtro ,
        success:(data)=> {
          document.querySelector(".lista").innerHTML=data
        }
      });

})
