var Utils = require('./Utils.js')
var storage = require('./storage.js')
var filtro=[]
var object ={
   "Filtrar":(obj,data)=>{
     var precio=0
     precio=obj.Precio.replace('$','').replace(",",'')
     if(data.ciudad==""){
        if(data.tipo==""){
         if(Number(precio)>=Number(data.precioMin) && Number(precio)<=Number(data.precioMax))
          filtro.push(obj)
        }
        else if(data.tipo==obj.Tipo){
           if(Number(precio)>=Number(data.precioMin) && Number(precio)<=Number(data.precioMax))
            filtro.push(obj)
        }
      }
    else
    {
        if(data.ciudad==obj.Ciudad){
          if(data.tipo==""){
           if(Number(precio)>=Number(data.precioMin) && Number(precio)<=Number(data.precioMax))
            filtro.push(obj)
          }
          else if(data.tipo==obj.Tipo){
            if(Number(precio)>=Number(data.precioMin) && Number(precio)<=Number(data.precioMax))
             filtro.push(obj)
          }
        }
    }
   },
   "Mostar_todo":(data,res)=>{
     filtro=[]
     if(data.todo=='true')
     {
      for (var i = 0; i < storage.data.length; i++) {
          res.write(Utils.generarHtml( storage.data[i]))
      }
     }
     else {
       for (var i = 0; i < storage.data.length; i++) {
         object.Filtrar(storage.data[i],data)
       }
       for (var i = 0; i < filtro.length; i++) {
           res.write(Utils.generarHtml( filtro[i]))
       }
     }
    res.end()
  },
  "Cargar_ciudad":(res)=>{
    var ciudades=storage.ObtenerCiudad()
    for (var i = 0; i < ciudades.length; i++) {
      res.write(Utils.generarOptions(ciudades[i],"ciudad"))
    }
    res.end()
  },
  "Cargar_tipo":(res)=>{
    var tipos=storage.ObtenerTipo()
    for (var i = 0; i < tipos.length; i++) {
      res.write(Utils.generarOptions(tipos[i],"tipo"))
    }
    res.end()
  }
}

module.exports=object
