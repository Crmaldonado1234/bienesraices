var http = require('http')
var express = require('express')
var bodyParser = require('body-parser')
var Routing = require('./requestRouting')

var port = 8082
var app = express()

var Server=http.createServer(app)
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use('/',Routing)
Server.listen(port, function(){
	console.log('Server is running on port '+port)
})
