var object={
  "generarHtml":(data)=>{
    var html=''
    html+='<div class="card horizontal">'
    html+='  <div class="card-image">'
    html+='    <img src="img/home.jpg">'
    html+='  </div>'
    html+='  <div class="card-stacked">'
    html+='    <div class="card-content">'
    html+='      <div>'
    html+=`        <b>Direccion: </b><p>${data.Direccion}</p>`
    html+='      </div>'
    html+='      <div>'
    html+=`        <b>Ciudad: </b><p>${data.Ciudad}</p>`
    html+='      </div>'
    html+='      <div>'
    html+=`        <b>Telefono: </b><p>${data.Telefono}</p>`
    html+='      </div>'
    html+='      <div>'
    html+=`        <b>Código postal: </b><p>${data.Codigo_Postal}</p>`
    html+='      </div>'
    html+='      <div>'
    html+=`        <b>Precio: </b><p>${data.Precio}</p>`
    html+='      </div>'
    html+='      <div>'
    html+=`        <b>Tipo: </b><p>${data.Tipo}</p>`
    html+='      </div>'
    html+='    </div>'
    html+='    <div class="card-action right-align">'
    html+='      <a href="#">Ver más</a>'
    html+='    </div>'
    html+='  </div>'
    html+='</div>'
    return html
  },
  "generarOptions":(data,combo)=>{

    return `<li><a href="#!" onclick="filtro.${combo}='${data}'">${data}</a></li>`
  }
}
module.exports=object
