var express = require('express')
var path = require('path')
var handler=require('./handlers.js')
var Router = express.Router()

var viewsPath = path.join(__dirname,'../')+'public/'
console.log('Router');
Router.get('/',function(req,res){
    res.sendFile(viewsPath+'index.html')
})
Router.get('/app.css',function(req,res){
  res.sendFile(viewsPath+'app.css')
})
Router.get('/app.js',function(req,res){
  res.sendFile(viewsPath+'app.js')
})
Router.get('/data.json',function(req,res){
  res.sendFile(viewsPath+'data.json')
})
Router.get('/ion.rangeSlider.css',function(req,res){
  res.sendFile(viewsPath+'ion.rangeSlider.css')
})
Router.get('/ion.rangeSlider.min.js',function(req,res){
  res.sendFile(viewsPath+'ion.rangeSlider.min.js')
})
Router.get('/ion.rangeSlider.skinFlat.css',function(req,res){
  res.sendFile(viewsPath+'ion.rangeSlider.skinFlat.css')
})
Router.get('/img/home.jpg',function(req,res){
  res.sendFile(viewsPath+'img/home.jpg')
})
Router.get('/img/sprite-skin-flat.png',function(req,res){
  res.sendFile(viewsPath+'img/sprite-skin-flat.png')
})
Router.post('/Mostar_todo',function(req,res){
  handler.Mostar_todo(req.body,res)
})
Router.post('/Cargar_ciudad',function(req,res){
  handler.Cargar_ciudad(res)
})
Router.post('/Cargar_tipo',function(req,res){
  handler.Cargar_tipo(res)
})
Router.all('*',function(req,res){
    res.send('No se encontró el recurso solicitado')
    res.end()
})
module.exports=Router
