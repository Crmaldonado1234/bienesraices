var data = require("../public/data.json");

var storage={
  data,
  Ciudad:[],
  Tipo:[],
  ObtenerCiudad:()=>{
    for (var i = 0; i < storage.data.length; i++) {
      if(storage.Ciudad.indexOf(storage.data[i].Ciudad)<0)
        storage.Ciudad.push(storage.data[i].Ciudad)
    }
    return storage.Ciudad
  },
  ObtenerTipo:()=>{
    for (var i = 0; i < storage.data.length; i++) {
      if(storage.Tipo.indexOf(storage.data[i].Tipo)<0)
        storage.Tipo.push(storage.data[i].Tipo)
    }
    return storage.Tipo
  }

}

module.exports=storage
